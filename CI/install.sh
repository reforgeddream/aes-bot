#!/bin/bash

# do some basic setup
echo US/Central > /etc/timezone

python --version  # for debug
pip --version

# create virtual environment on absolute path /venv
python -m venv /venv
# install all dependencies here
/venv/bin/pip install -r /tmp/requirements.txt

# remove temporary folder containing this script, requirements.txt and other
rm -rf /tmp/*